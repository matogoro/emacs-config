;;; project-supplement.el --- Supplementary customizations for `project.el' -*- no-byte-compile: t; lexical-binding: t; -*-

;;; * Modified commands

(setq project-list-file "/home/paul/Uniconfig/Settings/Emacs/etc/projects.el")

(defun matogoro/project-remember-this-project (pr)
  "Save project PR to the list of known projects.
Interactively, PR will be the current project."
  (interactive (list (project-current)))
  (unless pr
    (user-error "No project at this location"))
  (project-remember-project pr)
  (message "Saved current project to list"))
