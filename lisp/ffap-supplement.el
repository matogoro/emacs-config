;;; ffap-supplement.el --- Supplementary customizations for `find-file-at-point' (FFAP) -*- no-byte-compile: t; lexical-binding: t; -*-

;;; * Modified commands

(defun ffap-read-file-or-url (prompt guess)
  "Modification to the original `ffap-read-file-or-url' to remove the prompt."
  (or guess (setq guess default-directory))
  (let (dir)
    (or (ffap-url-p guess)
        (progn
          (or (ffap-file-remote-p guess)
              (setq guess
                    (abbreviate-file-name (expand-file-name guess))))
          (setq dir (file-name-directory guess))))
    (or (ffap-url-p guess) (setq guess (substitute-in-file-name guess)))
    guess))

(defun ffap-menu-ask (title alist cont)
  "Modification to the original `ffap-menu-ask' to remove `minibuffer-completion-help'."
  (let (choice)
    (cond
     ;; Emacs mouse:
     ((and (fboundp 'x-popup-menu)
           (listp last-nonmenu-event)
           last-nonmenu-event)
      (setq choice
            (x-popup-menu
             t
             (list "" (cons title
                            (mapcar (lambda (i) (cons (car i) i))
                                    alist))))))
     (t
      (setq choice
            (completing-read
             (format-prompt title (car (car alist)))
             alist nil t
             nil))
      (sit-for 0)
      (setq choice (or (assoc choice alist) (car alist)))))
    (if choice
        (funcall cont choice)
      (message "No choice made!")
      nil)))

(defun matogoro/ffap-dwim ()
  "Perform `find-file-at-point' at various locations.

If in `dired-mode', open externally with `xdg-open'.
If in `org-mode' and an `org-link' is present at point, follow the link.
Otherwise, perform `find-file-at-point'.  When `universal-argument' is called
first, perform either action in `other-window'."
  (interactive)
  (if (string= major-mode "dired-mode")
      (let* ((file (dired-get-filename nil t)))
        (message "Opening %s..." file)
        (call-process "xdg-open" nil 0 nil file)
        (message "Opening %s done" file))
    (if current-prefix-arg
        (if (and (string= major-mode "org-mode")
	             (string= (nth 0 (org-element-context)) "link"))
	        (let ((org-link-frame-setup '((file . find-file-other-window))))
	          (org-open-at-point))
	      (call-interactively 'ffap-other-window))
      (if (and (string= major-mode "org-mode")
	           (string= (nth 0 (org-element-context)) "link"))
	      (org-open-at-point)
        (if (and (string= major-mode "org-mode")
	             (or (string= (nth 0 (org-element-context)) "footnote-reference") (string= (nth 0 (org-element-context)) "footnote-definition")))
	        (org-open-at-point)
          (call-interactively 'find-file-at-point))))))

(defun matogoro/browse-url-at-point-firefox (&optional ARG)
(interactive)
(let ((url (browse-url-url-at-point)))
  (if url
	  (browse-url-firefox url ARG)
    (error "No URL found"))))
