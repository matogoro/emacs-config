;;; org-supplement.el --- Supplementary customizations for `org-mode' -*- no-byte-compile: t; lexical-binding: t; coding: utf-8;-*-

;;; Additional customizations for `org'

;;; * Subecosystem launchers

(defun matogoro/org-capture-at-point ()
  "Insert an org-capture template at point."
  (interactive)
  (org-capture 0))

(defun matogoro/org-noter ()
  "Launch org-noter with additional commands."
  (interactive)
  (org-noter)
  (matogoro/region-lines-unicolor))

;;; * Date-time string autogeneration for file modification time

(defun zp/org-find-time-file-property (property &optional anywhere)
  "Return the position of the time file PROPERTY if it exists.

When ANYWHERE is non-nil, search beyond the preamble."
  (save-excursion
    (goto-char (point-min))
    (let ((first-heading
           (save-excursion
             (re-search-forward org-outline-regexp-bol nil t))))
      (when (re-search-forward (format "^#\\+%s:" property)
                               (if anywhere nil first-heading)
                               t)
        (point)))))

(defun zp/org-has-time-file-property-p (property &optional anywhere)
  "Return the position of time file PROPERTY if it is defined.

As a special case, return -1 if the time file PROPERTY exists but
is not defined."
  (when-let ((pos (zp/org-find-time-file-property property anywhere)))
    (save-excursion
      (goto-char pos)
      (if (and (looking-at-p " ")
               (progn (forward-char)
                      (org-at-timestamp-p 'lax)))
          pos
        -1))))

(defun zp/org-set-time-file-property (property &optional anywhere pos)
  "Set the time file PROPERTY in the preamble.

When ANYWHERE is non-nil, search beyond the preamble.

If the position of the file PROPERTY has already been computed,
it can be passed in POS."
  (when-let ((pos (or pos
                      (zp/org-find-time-file-property property))))
    (save-excursion
      (goto-char pos)
      (if (looking-at-p " ")
          (forward-char)
        (insert " "))
      (delete-region (point) (line-end-position))
      (let* ((now (format-time-string "[%Y-%m-%d %a %H:%M]")))
        (insert now)))))

(defun zp/org-set-last-modified ()
  "Update the LAST_MODIFIED file property in the preamble."
  (when (derived-mode-p 'org-mode)
    (zp/org-set-time-file-property "LAST_MODIFIED")))

;;; * Word count functions

(defun matogoro/org-element-parse-headline-at-point (&optional granularity visible-only)
  "Parse current headline.
GRANULARITY and VISIBLE-ONLY are like the args of `org-element-parse-buffer'."
  (let ((level (org-current-level)))
    (org-element-map
	(org-element-parse-buffer granularity visible-only)
	'headline
      (lambda (el)
	(and
	 (eq (org-element-property :level el) level)
	 (<= (org-element-property :begin el) (point))
	 (<= (point) (org-element-property :end el))
	 el))
      nil 'first-match 'no-recursion)))

(defun matogoro/modify-syntax-table-org-simplified-wc ()
  (let ((custom-syntax-table (make-syntax-table)))
    (dolist (ch (list ?$ ?& ?* ?+ ?- ?_ ?< ?> ?= ?/ ?. ?, ?: ?\( ?\) ?\[ ?\] ?\{ ?\}))
      (modify-syntax-entry ch "w" custom-syntax-table))
    (with-syntax-table custom-syntax-table (count-words (point-min) (point-max)))))

(cl-defun matogoro/org-simplified-wc (&key (worthy '(paragraph bold italic underline code footnote-reference link strike-through subscript superscript table table-row table-cell))
					   (no-recursion nil))
  "Count words in the section of the current heading.
WORTHY is a list of things worthy to be counted.
This list should at least include the symbols:
paragraph, bold, italic, underline and strike-through.
If NO-RECURSION is non-nil don't count the words in subsections.
Universal argument counts the entire buffer."
  (interactive)
  (if (equal current-prefix-arg nil)
      (let ((word-count 0)
	    (char-count 0))
	(org-element-map
	    (org-element-contents (matogoro/org-element-parse-headline-at-point))
	    '(paragraph table)
	  (lambda (par)
	    (org-element-map
		par
		worthy
	      (lambda (el)
		(cl-incf
		 word-count
		 (cl-loop
		  for txt in (org-element-contents el)
		  when (eq (org-element-type txt) 'plain-text)
		  sum
		  (with-temp-buffer
		    (insert txt)
		    (matogoro/modify-syntax-table-org-simplified-wc)
		    )))
		(cl-incf
		 char-count
		 (cl-loop
		  for txt in (org-element-contents el)
		  when (eq (org-element-type txt) 'plain-text)
		  sum
		  (with-temp-buffer
		    (insert txt)
		    (setq char-count (- (point-max) (point-min))))))
		)))
	  nil nil (and no-recursion 'headline))
	(when (called-interactively-p 'any)
	  (message "[Org Section] Word count: %d | Character count: %d" word-count char-count))
	word-count char-count)
    (let ((word-count 0)
	  (char-count 0))
      (org-element-map
	  (org-element-parse-buffer)
	  '(paragraph table)
	(lambda (par)
	  (org-element-map
              par
              worthy
            (lambda (el)
              (cl-incf
               word-count
               (cl-loop
		for txt in (org-element-contents el)
		when (eq (org-element-type txt) 'plain-text)
		sum
		(with-temp-buffer
		  (insert txt)
		  (matogoro/modify-syntax-table-org-simplified-wc)
		  )))
	      (cl-incf
               char-count
               (cl-loop
		for txt in (org-element-contents el)
		when (eq (org-element-type txt) 'plain-text)
		sum
		(with-temp-buffer
		  (insert txt)
		  (setq char-count (- (point-max) (point-min))))))
	      )))
	nil nil (and no-recursion 'headline))
      (when (called-interactively-p 'any)
	(message "[Org Buffer] Word count: %d | Character count: %d" word-count char-count))
      word-count char-count)))


(defun matogoro/org-element-parse-headline-manuscript (&optional granularity visible-only)
  (if (equal current-prefix-arg nil)
      (let ((level 0))                       ; or restrict level
	    (org-element-map
            (org-element-parse-buffer granularity visible-only)
            'headline
	      (lambda (el)
            ;; eg. restrict elements to levels greater than `level`
            (when (< level (org-element-property :level el))
              (and
	           (org-element-property :BODY el)
               ;; match "body" tags
               ;; (member "body" (org-element-property :tags el))
               ;; ...
               el)))
	      ;; dont set 'first-match if you want all the matches
	      nil nil 'no-recursion))
    (let ((level 0))                       ; or restrict level
      (org-element-map
          (org-element-parse-buffer granularity visible-only)
          'headline
	    (lambda (el)
          ;; eg. restrict elements to levels greater than `level`
          (when (< level (org-element-property :level el))
            (and
	         (org-element-property :ABSTRACT el)
             ;; match "body" tags
             ;; (member "body" (org-element-property :tags el))
             ;; ...
             el)))
	    ;; dont set 'first-match if you want all the matches
	    nil nil 'no-recursion))))

(cl-defun matogoro/org-simplified-wc-manuscript (&key (worthy '(paragraph bold italic underline code footnote-reference link strike-through subscript superscript table table-row table-cell))
						                              (no-recursion nil))
  "Count words in the body sections of an Org manuscript buffer.
WORTHY is a list of things worthy to be counted.
This list should at least include the symbols:
paragraph, bold, italic, underline and strike-through.
If NO-RECURSION is non-nil don't count the words in subsections.
Universal argument counts the entire buffer."
  (interactive)
  (if (equal current-prefix-arg nil)
      (let ((word-count 0)
	        (char-count 0))
	    (org-element-map
	        (org-element-contents (matogoro/org-element-parse-headline-manuscript))
	        '(paragraph table)
	      (lambda (par)
	        (org-element-map
		        par
		        worthy
	          (lambda (el)
		        (cl-incf
		         word-count
		         (cl-loop
		          for txt in (org-element-contents el)
		          when (eq (org-element-type txt) 'plain-text)
		          sum
		          (with-temp-buffer
		            (insert txt)
		            (matogoro/modify-syntax-table-org-simplified-wc)
		            )))
		        (cl-incf
		         char-count
		         (cl-loop
		          for txt in (org-element-contents el)
		          when (eq (org-element-type txt) 'plain-text)
		          sum
		          (with-temp-buffer
		            (insert txt)
		            (setq char-count (- (point-max) (point-min))))))
		        )))
	      nil nil (and no-recursion 'headline)
	      )
	    (when (called-interactively-p 'any)
	      (message "[Org Manuscript Body] Word count: %d | Character count: %d" word-count char-count))
	    word-count char-count)
    (let ((word-count 0)
	      (char-count 0))
      (org-element-map
	      (org-element-contents (matogoro/org-element-parse-headline-manuscript))  
	      '(paragraph table)
	    (lambda (par)
	      (org-element-map
	          par
	          worthy
            (lambda (el)
	          (cl-incf
	           word-count
	           (cl-loop
		        for txt in (org-element-contents el)
		        when (eq (org-element-type txt) 'plain-text)
		        sum
		        (with-temp-buffer
		          (insert txt)
		          (matogoro/modify-syntax-table-org-simplified-wc)
		          )))
	          (cl-incf
	           char-count
	           (cl-loop
		        for txt in (org-element-contents el)
		        when (eq (org-element-type txt) 'plain-text)
		        sum
		        (with-temp-buffer
		          (insert txt)
		          (setq char-count (- (point-max) (point-min))))))
	          )))
	    nil nil (and no-recursion 'headline)
	    )
      (when (called-interactively-p 'any)
	    (message "[Org Manuscript Abstract] Word count: %d | Character count: %d" word-count char-count))
      word-count char-count)))

;;; * Org links

(defun matogoro/org-store-insert-link ()
  "Combines storing and inserting org links into one function.
Regular invocation runs `org-store-link'.
Universal argument runs `org-insert-link'."
  (interactive)
  (if (equal current-prefix-arg nil)
      (call-interactively 'org-store-link)
    (call-interactively 'org-insert-link)))

;;; * Org-cite

;; Patch to reduce lag when typing in lines with `org-cite' citations.
;; Basically, font-lock forces `org-cite' to calculate buffer hash repeatedly when typing.
;; The code below revises the `org-cite-basic--parse-bibliography' function to skip
;; buffer hash calculation when bibliography file is unchanged on disk.
;; Calculating buffer hash on every call is slow when the bibliography file is large.

(defvar org-cite-basic--file-id-cache nil
  "Hash table linking files to their hash.")

(defun org-cite-basic--parse-bibliography (&optional info)
  "List all entries available in the buffer.

Each association follows the pattern

  (FILE . ENTRIES)

where FILE is the absolute file name of the BibTeX file, and ENTRIES is a hash
table where keys are references and values are association lists between fields,
as symbols, and values as strings or nil.

Optional argument INFO is the export state, as a property list."
  (unless (hash-table-p org-cite-basic--file-id-cache)
    (setq org-cite-basic--file-id-cache (make-hash-table :test #'equal)))
  (if (plist-member info :cite-basic/bibliography)
      (plist-get info :cite-basic/bibliography)
    (let ((results nil))
      (dolist (file (org-cite-list-bibliography-files))
        (when (file-readable-p file)
          (with-temp-buffer
            (when (or (org-file-has-changed-p file)
                      (not (gethash file org-cite-basic--file-id-cache)))
              (insert-file-contents file)
              (puthash file (org-buffer-hash) org-cite-basic--file-id-cache))
            (let* ((file-id (cons file (gethash file org-cite-basic--file-id-cache)))
                   (entries
                    (or (cdr (assoc file-id org-cite-basic--bibliography-cache))
                        (let ((table
                               (pcase (file-name-extension file)
                                 ("json" (org-cite-basic--parse-json))
                                 ("bib" (org-cite-basic--parse-bibtex 
					 'biblatex))
                                 ("bibtex" (org-cite-basic--parse-bibtex 
					    'BibTeX))
                                 (ext
                                  (user-error "Unknown bibliography extension: 
%S"
                                              ext)))))
                          (push (cons file-id table) 
				org-cite-basic--bibliography-cache)
                          table))))
              (push (cons file entries) results)))))
      (when info (plist-put info :cite-basic/bibliography results))
      results)))

;;; * Archiving functions

(defun org-get-local-archive-location ()
  "Get the archive location applicable at point."
  (let ((re "^[ \t]*#\\+ARCHIVE:[ \t]+\\(\\S-.*\\S-\\)[ \t]*$")
	prop)
    (save-excursion
      (save-restriction
	(widen)
	(setq prop (org-entry-get nil "ARCHIVE" 'inherit))
	(cond
	 ((and prop (string-match "\\S-" prop))
	  prop)
	 ((or (re-search-backward re nil t)
              (re-search-forward re nil t))
	  (match-string 1))
	 (t org-archive-location))))))

(defun org-extract-archive-file (&optional location)
  "Extract and expand the file name from archive LOCATION.
if LOCATION is not given, the value of `org-archive-location' is used."
  (setq location (or location org-archive-location))
  (if (string-match "\\(.*\\)::\\(.*\\)" location)
      (if (= (match-beginning 1) (match-end 1))
          (buffer-file-name (buffer-base-buffer))
        (expand-file-name
         (format (match-string 1 location)
                 (file-name-nondirectory
                  (buffer-file-name (buffer-base-buffer))))))))

(defun org-extract-archive-heading (&optional location)
  "Extract the heading from archive LOCATION.
if LOCATION is not given, the value of `org-archive-location' is used."
  (setq location (or location org-archive-location))
  (if (string-match "\\(.*\\)::\\(.*\\)" location)
      (format (match-string 2 location)
              (file-name-nondirectory
               (buffer-file-name (buffer-base-buffer))))))

(defun org-archive-save-buffer ()
  (interactive)
  (let ((afile (org-extract-archive-file (org-get-local-archive-location))))
    (if (file-exists-p afile)
	(let ((buffer (find-file-noselect afile)))
          (with-current-buffer buffer
            (save-buffer)))
      (message "(%s) does not exist." afile))))

(defun matogoro/org-archive-and-save ()
  (interactive)
  (org-archive-subtree-default)
  (org-archive-save-buffer))

;;; * Source block commands

(defun matogoro/org-in-any-block-p ()
  "Return non-nil if the point is in any Org block.

The Org block can be *any*: src, example, verse, etc., even any
Org Special block.

This function is heavily adapted from `org-between-regexps-p'."
  (save-match-data
    (let ((pos (point))
          (case-fold-search t)
          (block-begin-re "^[[:blank:]]*#\\+begin_\\(?1:.+?\\)\\(?: .*\\)*$")
          (limit-up (save-excursion (outline-previous-heading)))
          (limit-down (save-excursion (outline-next-heading)))
          beg end)
      (save-excursion
        ;; Point is on a block when on BLOCK-BEGIN-RE or if
        ;; BLOCK-BEGIN-RE can be found before it...
        (and (or (org-in-regexp block-begin-re)
                 (re-search-backward block-begin-re limit-up :noerror))
             (setq beg (match-beginning 0))
             ;; ... and BLOCK-END-RE after it...
             (let ((block-end-re (concat "^[[:blank:]]*#\\+end_"
                                         (match-string-no-properties 1)
                                         "\\( .*\\)*$")))
               (goto-char (match-end 0))
               (re-search-forward block-end-re limit-down :noerror))
             (> (setq end (match-end 0)) pos)
             ;; ... without another BLOCK-BEGIN-RE in-between.
             (goto-char (match-beginning 0))
             (not (re-search-backward block-begin-re (1+ beg) :noerror))
             ;; Return value.
             (cons beg end))))))

(defun matogoro/org-split-block ()
  "Sensibly split the current Org block at point."
  (interactive)
  (if (matogoro/org-in-any-block-p)
      (save-match-data
        (save-restriction
          (widen)
          (let ((case-fold-search t)
                (at-bol (bolp))
                block-start
                block-end)
            (save-excursion
              (re-search-backward "^\\(?1:[[:blank:]]*#\\+begin_.+?\\)\\(?: .*\\)*$" nil nil 1)
              (setq block-start (match-string-no-properties 0))
              (setq block-end (replace-regexp-in-string
                               "begin_" "end_" ;Replaces "begin_" with "end_", "BEGIN_" with "END_"
                               (match-string-no-properties 1))))
            ;; Go to the end of current line, if not at the BOL
            (unless at-bol
              (end-of-line 1))
            (insert (concat (if at-bol "" "\n")
                            block-end
                            "\n\n"
                            block-start
                            (if at-bol "\n" "")))
            ;; Go to the line before the inserted "#+begin_ .." line
            (beginning-of-line (if at-bol -1 0)))))
    (message "Point is not in an Org block")))
