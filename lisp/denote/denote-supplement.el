;;; denote-supplement.el --- Supplementary customizations for `denote' -*- no-byte-compile: t; lexical-binding: t; -*-

;;; * Custom front matter

(setq denote-org-front-matter
      "#+TITLE: %s
#+AUTHOR: Paul H. McClelland
#+DATE: %s
#+LAST_MODIFIED: 
#+FILETAGS: %s
#+IDENTIFIER: %s
#+SIGNATURE: %s
#+STARTUP: showall
\n")

;;; * `citar-denote' variables

(setq citar-denote-file-types
      `((org
         :reference-format "#+REFERENCE: %s\n"
         :reference-regex "^#\\+REFERENCE\\s-*:")
        (markdown-yaml
         :reference-format "REFERENCE: %s\n"
         :reference-regex "^REFERENCE\\s-*:")
        (markdown-toml
         :reference-format "REFERENCE = %s\n"
         :reference-regex "^REFERENCE\\s-*=")
        (text
         :reference-format "REFERENCE: %s\n"
         :reference-regex "^REFERENCE\\s-*:")))

;;; * Folgezettel functions

(defun matogoro/denote-fz-add-signature (&optional file variation)
  (interactive) 
  "Add a signature to FILE or the current's buffer unnumbered note.
 A prompt asks for a target note and VARIATION describes which new signature is created from the target note.

Modified so that a fifth DATE argument is provided for `denote-rename-file'."
  (let* ((file  (or file (dired-get-filename nil t) (buffer-file-name)))
	     (file-type (denote-filetype-heuristics file))
	     (title (denote-retrieve-title-value file file-type))
	     (keywords (denote-retrieve-keywords-value file file-type))
	     (current-signature  (denote-retrieve-filename-signature file))
	     (target (denote-fz-find-file))
	     (signature  (if variation
			             (denote-fz-find-valid-signature (denote-fz-derived-signature variation target))
		               (completing-read "Signature:"
					                    (list (denote-fz-find-valid-signature (denote-fz-derived-signature 'child target))
					                          (denote-fz-find-valid-signature (denote-fz-derived-signature 'sibling target))))))
         (date ))
    (if (equal "0" current-signature)
	    (denote-rename-file file title keywords signature (denote-valid-date-p (denote-retrieve-filename-identifier file)))
      (message "Not an unnumbered note."))))

;;; * Function for sorted `dired' buffer

(defun matogoro/denote-fz-dired-sorted ()
  (interactive)
  (dired "~/Documents/Zettelkasten/Notes/Topic/")
  (denote-fz-dired-sorted "*"))

;;; * Create a local `./img/' directory for `org-download' named after the `denote-identifier'

(defun matogoro/org-download-denote-dir-create ()
  "Create directory ./img/$DENOTE_IDENTIFIER/"
  (interactive)
  (make-directory (concat "./img/" (denote-retrieve-filename-identifier (buffer-file-name)) "/")))
