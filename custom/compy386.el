(add-to-list 'custom-theme-load-path "~/.config/base16/el/")

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:inherit nil :stipple nil :foreground "#E0E0E0" :inverse-video nil :box nil :strike-through nil :overline nil :underline nil :slant normal :weight normal :height 70 :width normal :foundry "UKWN" :family "mononoki"))))
 '(bold ((t (:weight ultra-bold))))
 '(cursor ((t (:background "#FFA07A"))))
 '(fringe ((t (:background "#202A2F"))))
 '(help-key-binding ((t (:foreground "#97F2E7" :background "#292929" :family "mononoki" :box (:line-width (1 . 1) :style released-button)))))
 '(hl-line ((t (:background "#333333"))))
 '(line-number ((t (:foreground "gray47" :background "#202A2F"))))
 '(menu ((t (:background "#202A2F" :foreground "#E7E7E7" :height 1.0 :family "mononoki"))))
 '(mouse ((t (:background "white smoke" :foreground "dim gray"))))
 '(org-block ((t (:background "#1B252A" :extend t))))
 '(org-block-begin-line ((t (:background "#1D272C" :foreground "#475944" :underline "#2A3439" :extend t))))
 '(org-block-end-line ((t (:background "#1D272C" :foreground "#475944" :overline "#2A3439" :extend t))))
 '(org-hide ((t (:background "#202A2F" :foreground "#202A2F")))))
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   '("dfe0fa8869db18eeb4cbe95c6e9dbc6125b30af2b869c32dbb9fa164e041237f" "1bb5519e9e17700eb0e0032f6c3b3eb24021024582358f53748abc89b6681076" default))
 '(org-agenda-files
   '("~/Uniconfig/Settings/Emacs/init.org" "/home/paul/Documents/Orgzly/ANCILLARY.org" "/home/paul/Documents/Orgzly/DASHBOARD.org"))
 '(safe-local-variable-values
   '((eval setq org-download-image-dir
           (concat "./img/"
                   (denote-retrieve-filename-identifier
                    (buffer-file-name))
                   "/")))))
