;;; consult-omni-supplement.el --- Supplementary customizations for `consult-omni' -*- no-byte-compile: t; lexical-binding: t; -*-

(setq consult-omni-pubmed-api-key (matogoro/api-key-pubmed))
(setq consult-omni-scopus-api-key (matogoro/api-key-scopus))

;; * Custom multi-source commands

(setq consult-omni-scholar-sources (list "PubMed" "doiorg" "Scopus" "Wikipedia"))

(defun matogoro/consult-omni-scholar (&optional initial prompt sources no-callback &rest args)
  "Interactive 'multi-source academic literature' search.
This is similar to `consult-omni-multi', but it runs the search on
academic literature sources defined in `consult-omni-scholar-sources'.
See `consult-omni-multi' for more details."
  (interactive "P")
  (let ((prompt (or prompt (concat "[" (propertize "Academic 🎓" 'face 'consult-omni-prompt-face) "]" " Keywords or DOI:  ")))
	(sources (or sources consult-omni-scholar-sources)))
    (consult-omni-multi-static initial prompt sources no-callback args)))

;; * Special functions

(defun matogoro/eww-browse-url-delay-readable (url)
  "Hacky method to automatically invoke EWW readable mode in a delayed manner.
This is needed for some redirected web pages since `(add-hook 'eww-after-render-hook 'eww-readable)' will interfere with page rendering.
Particularly useful for previews of web pages with `consult'."
  (eww-browse-url url)
  (sleep-for 1)
  (eww-readable))
