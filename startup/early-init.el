;;; early-init.el --- Early Init -*- no-byte-compile: t; lexical-binding: t; -*-

;; This file contains early init optimizations for Emacs startup.  As such, it
;; should be symlinked to the appropriate location to be read by Emacs
;; prior to loading the GUI (per the Emacs manual, either ~/.config/emacs/ or
;; ~/.emacs.d/).  It is primarily kept in its current location for
;; organizational and version-control purposes.

;; This file is based on the `early-init.el' file within the `minimal-emacs'
;; project by James Cherti (https://github.com/jamescherti/minimal-emacs.d).

;;; * Variables

(defvar base-setup-ui-features '()
  "List of user interface features to disable in minimal Emacs setup.

This variable holds a list Emacs UI features that can be enabled:
- `context-menu`: Enables the context menu in graphical environments.
- `tool-bar`: Enables the tool bar in graphical environments.
- `menu-bar`: Enables the menu bar in graphical environments.
- `dialogs`: Enables both file dialogs and dialog boxes.
- `tooltips`: Enables tooltips.

Each feature in the list corresponds to a specific UI component that can be
turned on.")

(defvar base-setup-frame-title-format "%b – Emacs"
  "Template for displaying the title bar of visible and iconified frame.")

(defvar base-setup-debug nil
  "Non-nil to enable debug.")

(defvar base-setup-gc-cons-threshold (* 16 1024 1024)
  "The value of `gc-cons-threshold' after Emacs startup.")

;;; * Inhibit package.el

(setq package-enable-at-startup nil)

;;; * Configure Emacs directory

(setq user-emacs-directory (expand-file-name "~/Uniconfig/Settings/Emacs/"))

;;; * Garbage collection
;; Garbage collection significantly affects startup times. This setting delays
;; garbage collection during startup but will be reset later.

(setq gc-cons-threshold most-positive-fixnum)

(add-hook 'emacs-startup-hook
          (lambda ()
            (setq gc-cons-threshold base-setup-gc-cons-threshold)))

;;; * Disable native compilation

;; While native compilation is designed to speed up Emacs boot time by generating
;; per-computer compiled files, in practice these benefits are negligible
;; compared to standard byte-compiling on the computers used in this configuration
;; (i.e., well within the standard startup time variance).  Moreover, native
;; compilation has also triggered errors in testing when porting this Emacs
;; configuration to different devices, and it creates overhead whenever new
;; packages are installed.  As such, it is disabled for now.

(setq native-comp-deferred-compilation nil)

;; If at some point native compilation becomes faster or more usable between
;; devices, the following code sets up a local native compilation directory
;; to be established on a per-computer basis.

;; (when (featurep 'native-compile)
;;   ;; Set a local directory to store the native compilation cache
;;   (let ((path "~/.emacs.d/eln-cache/"))
;; 	(setq-default native-comp-eln-load-path (list path)
;; 				  native-compile-target-directory path)
;; 	(startup-redirect-eln-cache path))
;; 
;;   (setq-default native-comp-async-report-warnings-errors nil  ;; Silence compiler warnings
;; 				native-comp-jit-compilation              t    ;; Enable async native compilation
;; 				package-native-compile                   t)) ;; Compile installed packages

;;; * Default frame color

(add-to-list 'custom-theme-load-path (expand-file-name "custom/themes" user-emacs-directory))
(load-theme 'matogoro t)

;;; * Language environment

(set-language-environment "UTF-8")

;; Set-language-environment sets default-input-method, which is unwanted.
(setq default-input-method nil)

;;; * Performance

;; Prefer loading newer compiled files
(setq load-prefer-newer t)

;; Font compacting can be very resource-intensive, especially when rendering
;; icon fonts on Windows. This will increase memory usage.
(setq inhibit-compacting-font-caches t)

(unless noninteractive
  (unless base-setup-debug
    (unless base-setup-debug
      ;; Suppress redisplay and redraw during startup to avoid delays and
      ;; prevent flashing an unstyled Emacs frame.
      ;; (setq-default inhibit-redisplay t) ; Can cause artifacts
      ;; (setq-default inhibit-message t)

      ;; Reset the above variables to prevent Emacs from appearing frozen or
      ;; visually corrupted after startup or if a startup error occurs.
      (defun base-setup--reset-inhibited-vars-h ()
        ;; (setq-default inhibit-redisplay nil) ; Can cause artifacts
        (setq-default inhibit-message nil)
        (remove-hook 'post-command-hook #'base-setup--reset-inhibited-vars-h))

      (add-hook 'post-command-hook
                #'base-setup--reset-inhibited-vars-h -100))

    (dolist (buf (buffer-list))
      (with-current-buffer buf
        (setq mode-line-format nil)))

    (put 'mode-line-format 'initial-value
         (default-toplevel-value 'mode-line-format))
    (setq-default mode-line-format nil)

    ;; Without this, Emacs will try to resize itself to a specific column size
    (setq frame-inhibit-implied-resize t)

    ;; A second, case-insensitive pass over `auto-mode-alist' is time wasted.
    ;; No second pass of case-insensitive search over auto-mode-alist.
    (setq auto-mode-case-fold nil)

    ;; Reduce *Message* noise at startup. An empty scratch buffer (or the
    ;; dashboard) is more than enough, and faster to display.
    (setq inhibit-startup-screen t
          inhibit-startup-echo-area-message user-login-name)
    (setq initial-buffer-choice nil
          inhibit-startup-buffer-menu t
          inhibit-x-resources t)

    ;; Disable bidirectional text scanning for a modest performance boost.
    (setq-default bidi-display-reordering 'left-to-right
                  bidi-paragraph-direction 'left-to-right)

    ;; Give up some bidirectional functionality for slightly faster re-display.
    (setq bidi-inhibit-bpa t)

    ;; Remove "For information about GNU Emacs..." message at startup
    (advice-add #'display-startup-echo-area-message :override #'ignore)

    ;; Suppress the vanilla startup screen completely. We've disabled it with
    ;; `inhibit-startup-screen', but it would still initialize anyway.
    (advice-add #'display-startup-screen :override #'ignore)

    ;; Shave seconds off startup time by starting the scratch buffer in
    ;; `fundamental-mode'
    (setq initial-major-mode 'fundamental-mode
          initial-scratch-message nil)

    (unless base-setup-debug
      ;; Unset command line options irrelevant to the current OS. These options
      ;; are still processed by `command-line-1` but have no effect.
      (unless (eq system-type 'darwin)
        (setq command-line-ns-option-alist nil))
      (unless (memq initial-window-system '(x pgtk))
        (setq command-line-x-option-alist nil)))))

;;; UI elements

(setq frame-title-format base-setup-frame-title-format
      icon-title-format base-setup-frame-title-format)

;; Disable startup screens and messages
(setq inhibit-splash-screen t)

;; Intentionally avoid calling `menu-bar-mode', `tool-bar-mode', and
;; `scroll-bar-mode' because manipulating frame parameters can trigger or queue
;; a superfluous and potentially expensive frame redraw at startup, depending
;; on the window system. The variables must also be set to `nil' so users don't
;; have to call the functions twice to re-enable them.
(unless (memq 'menu-bar base-setup-ui-features)
  (push '(menu-bar-lines . 0) default-frame-alist)
  (unless (memq window-system '(mac ns))
    (setq menu-bar-mode nil)))

(unless (daemonp)
  (unless noninteractive
    (when (fboundp 'tool-bar-setup)
      ;; Temporarily override the tool-bar-setup function to prevent it from
      ;; running during the initial stages of startup
      (advice-add #'tool-bar-setup :override #'ignore)
      (define-advice startup--load-user-init-file
          (:after (&rest _) base-setup-setup-toolbar)
        (advice-remove #'tool-bar-setup #'ignore)
        (when tool-bar-mode
          (tool-bar-setup))))))
(unless (memq 'tool-bar base-setup-ui-features)
  (push '(tool-bar-lines . 0) default-frame-alist)
  (setq tool-bar-mode nil))

(push '(vertical-scroll-bars) default-frame-alist)
(push '(horizontal-scroll-bars) default-frame-alist)
(setq scroll-bar-mode nil)
(when (fboundp 'horizontal-scroll-bar-mode)
  (horizontal-scroll-bar-mode -1))

(unless (memq 'tooltips base-setup-ui-features)
  (when (bound-and-true-p tooltip-mode)
    (tooltip-mode -1)))

;; Disable GUIs because they are inconsistent across systems, desktop
;; environments, and themes, and they don't match the look of Emacs.
(unless (memq 'dialogs base-setup-ui-features)
  (setq use-file-dialog nil)
  (setq use-dialog-box nil))

(provide 'early-init)

;;; early-init.el ends here
