;;; abbrev.el --- Definitions for `abbrev-mode' -*- no-byte-compile: t; lexical-binding: t; coding: utf-8;-*-
(define-abbrev-table 'Buffer-menu-mode-abbrev-table '())

(define-abbrev-table 'Custom-mode-abbrev-table '())

(define-abbrev-table 'Info-edit-mode-abbrev-table '())

(define-abbrev-table 'Rd-mode-abbrev-table
  '(("`ag" "\\arguments")
    ("`al" "\\alias")
    ("`au" "\\author")
    ("`bf" "\\bold")
    ("`co" "\\code")
    ("`de" "\\describe")
    ("`dn" "\\description")
    ("`dt" "\\details")
    ("`em" "\\emph")
    ("`en" "\\enumerate")
    ("`ex" "\\examples")
    ("`fi" "\\file")
    ("`fo" "\\format")
    ("`it" "\\item")
    ("`iz" "\\itemize")
    ("`kw" "\\keyword")
    ("`li" "\\link")
    ("`me" "\\method")
    ("`na" "\\name")
    ("`no" "\\note")
    ("`re" "\\references")
    ("`sa" "\\seealso")
    ("`se" "\\section")
    ("`so" "\\source")
    ("`ss" "\\subsection")
    ("`sy" "\\synopsis")
    ("`ta" "\\tabular")
    ("`ti" "\\title")
    ("`us" "\\usage")
    ("`va" "\\value")))

(define-abbrev-table 'TeX-error-overview-mode-abbrev-table '())

(define-abbrev-table 'TeX-output-mode-abbrev-table '())

(define-abbrev-table 'awk-mode-abbrev-table
  '(
   ))

(define-abbrev-table 'bibtex-mode-abbrev-table '())

(define-abbrev-table 'bookmark-bmenu-mode-abbrev-table '())

(define-abbrev-table 'bookmark-edit-annotation-mode-abbrev-table '())

(define-abbrev-table 'c++-mode-abbrev-table
  '(
   ))

(define-abbrev-table 'c-mode-abbrev-table
  '(
   ))

(define-abbrev-table 'calc-trail-mode-abbrev-table '())

(define-abbrev-table 'calendar-mode-abbrev-table '())

(define-abbrev-table 'comint-mode-abbrev-table '())

(define-abbrev-table 'completion-list-mode-abbrev-table '())

(define-abbrev-table 'context-mode-abbrev-table '())

(define-abbrev-table 'debugger-mode-abbrev-table '())

(define-abbrev-table 'diff-mode-abbrev-table '())

(define-abbrev-table 'doctex-mode-abbrev-table '())

(define-abbrev-table 'edit-abbrevs-mode-abbrev-table '())

(define-abbrev-table 'elisp-byte-code-mode-abbrev-table '())

(define-abbrev-table 'emacs-lisp-mode-abbrev-table '())

(define-abbrev-table 'epa-info-mode-abbrev-table '())

(define-abbrev-table 'epa-key-list-mode-abbrev-table '())

(define-abbrev-table 'epa-key-mode-abbrev-table '())

(define-abbrev-table 'ert-results-mode-abbrev-table '())

(define-abbrev-table 'ert-simple-view-mode-abbrev-table '())

(define-abbrev-table 'eshell-mode-abbrev-table '())

(define-abbrev-table 'evil-command-window-mode-abbrev-table '())

(define-abbrev-table 'evil-list-view-mode-abbrev-table '())

(define-abbrev-table 'flymake-diagnostics-buffer-mode-abbrev-table '())

(define-abbrev-table 'fundamental-mode-abbrev-table '())

(define-abbrev-table 'global-abbrev-table
  '(("8Alpha" "Α")
    ("8Beta" "Β")
    ("8Gamma" "Γ")
    ("8Delta" "Δ")
    ("8Epsilon" "Ε")
    ("8Zeta" "Ζ")
    ("8Eta" "Η")
    ("8Theta" "Θ")
    ("8Iota" "Ι")
    ("8Kappa" "Κ")
    ("8Lambda" "Λ")
    ("8Mu" "Μ")
    ("8Nu" "Ν")
    ("8Xi" "Ξ")
    ("8Omicron" "Ο")
    ("8Pi" "Π")
    ("8Rho" "Ρ")
    ("8Sigma" "Σ")
    ("8Tau" "Τ")
    ("8Upsilon" "Υ")
    ("8Phi" "Φ")
    ("8Chi" "Χ")
    ("8Psi" "Ψ")
    ("8Omega" "Ω")
    ("8alpha" "α")
    ("8beta" "β")
    ("8gamma" "γ")
    ("8delta" "δ")
    ("8epsilon" "ε")
    ("8zeta" "ζ")
    ("8eta" "η")
    ("8theta" "θ")
    ("8iota" "ι")
    ("8kappa" "κ")
    ("8lambda" "λ")
    ("8mu" "μ")
    ("8nu" "ν")
    ("8xi" "ξ")
    ("8omicron" "ο")
    ("8pi" "π")
    ("8rho" "ρ")
    ("8sigma" "σ")
    ("8varsigma" "ς")
    ("8tau" "τ")
    ("8upsilon" "υ")
    ("8phi" "φ")
    ("8chi" "χ")
    ("8psi" "ψ")
    ("8omega" "ω")
    ("8nab" "∇")
    ("8al" "←")
    ("8ar" "→")
    ("8au" "↑")
    ("8ad" "↓")
    ("8lal" "⟵")
    ("8lar" "⟶")
    ("8wal" "⇦")
    ("8war" "⇨")
    ("8wau" "⇧")
    ("8wad" "⇩")
    ("8bal" "⬅")
    ("8bar" "➡")
    ("8bau" "⬆")
    ("8bad" "⬇")
    ("8ssal" "🡐")
    ("8ssar" "🡒")
    ("8ssau" "🡑")
    ("8ssad" "🡓")
    ("8ah" "↔")
    ("8av" "↕")
    ("8xal" "↚")
    ("8xar" "↛")
    ("8xah" "↮")
    ("8wah" "⬄")
    ("8wav" "⇳")
    ("8bah" "⬌")
    ("8bav" "⬍")
    ("8ssah" "🡘")
    ("8ssav" "🡙")
    ("8anw" "↖")
    ("8ane" "↗")
    ("8ase" "↘")
    ("8asw" "↙")
    ("8anwse" "⤡")
    ("8anesw" "⤢")
    ("8wanw" "⬁")
    ("8wane" "⬀")
    ("8wase" "⬂")
    ("8wasw" "⬃")
    ("8banw" "⬉")
    ("8bane" "⬈")
    ("8base" "⬊")
    ("8basw" "⬋")
    ("8ssanw" "🡔")
    ("8ssane" "🡕")
    ("8ssase" "🡖")
    ("8ssasw" "🡗")
    ("8hal" "🡄")
    ("8har" "🡆")
    ("8hau" "🡅")
    ("8had" "🡇")
    ("8qal" "🠘")
    ("8qar" "🠚")
    ("8qau" "🠙")
    ("8qad" "🠛")
    ("8qlal" "🠜")
    ("8qlar" "🠞")
    ("8qlau" "🠝")
    ("8qlad" "🠟")
    ("8tal" "⭠")
    ("8tar" "⭢")
    ("8tau" "⭡")
    ("8tad" "⭣")
    ("8tah" "⭤")
    ("8tav" "⭥")
    ("8tanw" "⭦")
    ("8tane" "⭧")
    ("8tase" "⭨")
    ("8tasw" "⭩")
    ("8talsh" "🠀")
    ("8tarsh" "🠂")
    ("8taush" "🠁")
    ("8tadsh" "🠃")
    ("8talmh" "🠄")
    ("8tarmh" "🠆")
    ("8taumh" "🠅")
    ("8tadmh" "🠇")
    ("8tallh" "🠈")
    ("8tarlh" "🠊")
    ("8taulh" "🠉")
    ("8tadlh" "🠋")
    ("8talls" "🠠")
    ("8tarls" "🠢")
    ("8tauls" "🠡")
    ("8tadls" "🠣")
    ("8talns" "🠤")
    ("8tarns" "🠦")
    ("8tauns" "🠥")
    ("8tadns" "🠧")
    ("8talms" "🠨")
    ("8tarms" "🠪")
    ("8taums" "🠩")
    ("8tadms" "🠫")
    ("8talhs" "🠬")
    ("8tarhs" "🠮")
    ("8tauhs" "🠭")
    ("8tadhs" "🠯")
    ("8talvhs" "🠰")
    ("8tarvhs" "🠲")
    ("8tauvhs" "🠱")
    ("8tadvhs" "🠳")
    ("8wiall" "🡠")
    ("8wiarl" "🡢")
    ("8wiaul" "🡡")
    ("8wiadl" "🡣")
    ("8wianwl" "🡤")
    ("8wianel" "🡥")
    ("8wiasel" "🡦")
    ("8wiaswl" "🡧")
    ("8wialn" "🡨")
    ("8wiarn" "🡪")
    ("8wiaun" "🡩")
    ("8wiadn" "🡫")
    ("8wianwn" "🡬")
    ("8wianen" "🡭")
    ("8wiasen" "🡮")
    ("8wiaswn" "🡯")
    ("8wialm" "🡰")
    ("8wiarm" "🡲")
    ("8wiaum" "🡱")
    ("8wiadm" "🡳")
    ("8wianwm" "🡴")
    ("8wianem" "🡵")
    ("8wiasem" "🡶")
    ("8wiaswm" "🡷")
    ("8wialh" "🡸")
    ("8wiarh" "🡺")
    ("8wiauh" "🡹")
    ("8wiadh" "🡻")
    ("8wianwh" "🡼")
    ("8wianeh" "🡽")
    ("8wiaseh" "🡾")
    ("8wiaswh" "🡿")
    ("8wialvh" "🢀")
    ("8wiarvh" "🢂")
    ("8wiauvh" "🢁")
    ("8wiadvh" "🢃")
    ("8wianwvh" "🢄")
    ("8wianevh" "🢅")
    ("8wiasevh" "🢆")
    ("8wiaswvh" "🢇")
    ("8chal" "⮜")
    ("8char" "⮞")
    ("8chau" "⮝")
    ("8chad" "⮟")
    ("8chalbw" "⮘")
    ("8charbw" "⮚")
    ("8chaubw" "⮙")
    ("8chadbw" "⮛")
    ("8bpl" "◄")
    ("8bpr" "►")
    ("8wpl" "◅")
    ("8wpr" "▻")
    ("8trl" "◀")
    ("8trr" "▶")
    ("8tru" "▲")
    ("8trd" "▼")
    ("8trlm" "⯇")
    ("8trrm" "⯈")
    ("8trum" "⯅")
    ("8trdm" "⯆")
    ("8hual" "↼")
    ("8huar" "⇀")
    ("8hdal" "↽")
    ("8hdar" "⇁")
    ("8hlau" "↿")
    ("8hrau" "↾")
    ("8hlad" "⇃")
    ("8hrad" "⇂")
    ("8huah" "⥊")
    ("8hdah" "⥋")
    ("8hlav" "⥌")
    ("8hrav" "⥍")
    ("8dal" "⇐")
    ("8dar" "⇒")
    ("8dau" "⇑")
    ("8dad" "⇓")
    ("8dah" "⇔")
    ("8dav" "⇕")
    ("8danw" "⇖")
    ("8dane" "⇗")
    ("8dase" "⇘")
    ("8dasw" "⇙")
    ("8dxal" "⇍")
    ("8dxar" "⇏")
    ("8dxah" "⇎")
    ("8dlal" "⟸")
    ("8dlar" "⟹")
    ("8dlah" "⟺")
    ("8inf" "∞")
    ("8deg" "°")
    ("8dash" "—")
    ("8pm" "±")
    ("8ge" "≥")
    ("8le" "≤")
    ("8ne" "≠")
    ("8ae" "≈")
    ("8equiv" "≡")
    ("8congru" "≅")
    ("8ncongru" "≇")
    ("8union" "∪")
    ("8intersec" "∩")
    ("8ele" "∈")
    ("8nele" "∉")
    ("8tf" "∴")
    ("8mul" "×")
    ("8div" "÷")
    ("8e" "ᴇ")
    ("8int" "∫")
    ("8dint" "∬")
    ("8tint" "∭")
    ("8cint" "∮")
    ("8cdint" "∯")
    ("8ctint" "∰")
    ("8dag" "†")
    ("8ddag" "‡")
    ("8sect" "§")
    ("8bul" "•")))

(define-abbrev-table 'gnus-group-mode-abbrev-table '())

(define-abbrev-table 'help-mode-abbrev-table '())

(define-abbrev-table 'ibuffer-mode-abbrev-table '())

(define-abbrev-table 'idl-mode-abbrev-table '())

(define-abbrev-table 'java-mode-abbrev-table '())

(define-abbrev-table 'latex-mode-abbrev-table '())

(define-abbrev-table 'lisp-mode-abbrev-table '())

(define-abbrev-table 'message-mode-abbrev-table '())

(define-abbrev-table 'messages-buffer-mode-abbrev-table '())

(define-abbrev-table 'objc-mode-abbrev-table '())

(define-abbrev-table 'occur-edit-mode-abbrev-table '())

(define-abbrev-table 'occur-mode-abbrev-table '())

(define-abbrev-table 'org-export-stack-mode-abbrev-table '())

(define-abbrev-table 'org-mode-abbrev-table '())

(define-abbrev-table 'outline-mode-abbrev-table '())

(define-abbrev-table 'package-menu-mode-abbrev-table '())

(define-abbrev-table 'pike-mode-abbrev-table '())

(define-abbrev-table 'plain-tex-mode-abbrev-table '())

(define-abbrev-table 'process-menu-mode-abbrev-table '())

(define-abbrev-table 'prog-mode-abbrev-table '())

(define-abbrev-table 'shell-mode-abbrev-table '())

(define-abbrev-table 'special-mode-abbrev-table '())

(define-abbrev-table 'speedbar-mode-abbrev-table '())

(define-abbrev-table 'tablist-mode-abbrev-table '())

(define-abbrev-table 'tabulated-list-mode-abbrev-table '())

(define-abbrev-table 'tar-mode-abbrev-table '())

(define-abbrev-table 'texinfo-mode-abbrev-table '())

(define-abbrev-table 'text-mode-abbrev-table '())

(define-abbrev-table 'url-cookie-mode-abbrev-table '())

(define-abbrev-table 'vc-git-log-edit-mode-abbrev-table '())

(define-abbrev-table 'vc-git-log-view-mode-abbrev-table '())

(define-abbrev-table 'vc-git-region-history-mode-abbrev-table '())

(define-abbrev-table 'xref--xref-buffer-mode-abbrev-table '())

