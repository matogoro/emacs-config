;;; notmuch-supplement.el --- Supplementary customizations for `notmuch' -*- no-byte-compile: t; lexical-binding: t; -*-

;;; * Generic functions

(defun matogoro/notmuch-refresh-this-buffer-autoclean ()
  "Run additional notmuch cleaning commands when refreshing the buffer."
  (interactive)
  (shell-command "notmuch tag -delayed -- tag:delayed and tag:deleted")
  (notmuch-refresh-this-buffer))

(defun matogoro/jump-to-notmuch-inbox ()
  "Jump directly to the notmuch inbox."
  (interactive)
  (notmuch-tree "tag:inbox"))

(defun matogoro/notmuch-tree-show-kill-message ()
  "Jump directly to the notmuch inbox."
  (interactive)
  (windmove-down)
  (notmuch-bury-or-kill-this-buffer))

;;; * Empty subject check

(defun matogoro/notmuch-mua-empty-subject-check ()
  "Request confirmation before sending a message with empty subject."
  (when (and (null (message-field-value "Subject"))
             (not (y-or-n-p "Subject is empty.  Send anyway? ")))
    (error "Sending message canceled: empty subject.")))

;;; * Empty attachment check

(defun matogoro/notmuch-mua-attachment-check ()
  "Signal an error an attachement is expected but missing.

Signal an error if the message text indicates that an attachment
is expected but no MML referencing an attachment is found.

Typically this is added to `notmuch-mua-send-hook'."
  (when (and
	     ;; When the message mentions attachment...
	     (save-excursion
	       (message-goto-body)
	       ;; Limit search from reaching other possible parts of the message
	       (let ((search-limit (search-forward "\n<#" nil t)))
	         (message-goto-body)
	         (cl-loop while (re-search-forward notmuch-mua-attachment-regexp
					                           search-limit t)
		              ;; For every instance of the "attachment" string
		              ;; found, examine the text properties. If the text
		              ;; has either a `face' or `syntax-table' property
		              ;; then it is quoted text and should *not* cause the
		              ;; user to be asked about a missing attachment.
		              if (let ((props (text-properties-at (match-beginning 0))))
			               (not (or (memq 'syntax-table props)
				                    (memq 'face props))))
		              return t
		              finally return nil)))
	     ;; ...but doesn't have a part with a filename...
	     (save-excursion
	       (message-goto-body)
	       (not (re-search-forward "^<#part [^>]*filename=" nil t)))
	     ;; ...and that's not okay...
	     (not (y-or-n-p "Attachment mentioned, but not included.  Send anyway? ")))
    ;; ...signal an error.
    (error "Missing attachment")))

;;; * `notmuch-delay'

(defun killing-new-buffers (&rest body)
  "Run BODY and kill any buffers that were not already open."
  (cl-with-gensyms (initial-buffers) 
    `(let ((,initial-buffers (buffer-list)))
       (unwind-protect
           ,(macroexp-progn body)
         (dolist (b (buffer-list)) (unless (memq b ,initial-buffers) (kill-buffer b)))))))
