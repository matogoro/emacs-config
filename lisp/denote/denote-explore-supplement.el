;;; denote-explore-supplement.el --- Supplementary customizations for `denote-explore' -*- no-byte-compile: t; lexical-binding: t; -*-

(setq denote-explore-network-graph-formats
      '((graphviz :file-extension ".gv" :encode denote-explore-network-encode-graphviz :display denote-explore-network-display-graphviz)
        (d3.js :file-extension ".json" :encode denote-explore-network-encode-json :display denote-explore-network-display-json)
        (gexf :file-extension ".gexf" :encode denote-explore-network-encode-gexf :display matogoro/denote-explore-network-display-gexf)))

(defun denote-explore-network-display-graphviz (gv-file)
  "Convert GraphViz GV-FILE to an SVG file and display in external application.
Output is saved to the `denote-explore-network-directory' in the
`denote-explore-network-graphviz-filetype' file format."
  (let* ((file-type denote-explore-network-graphviz-filetype)
	     (output-file (expand-file-name (concat denote-explore-network-filename "."
						                        file-type)
					                    denote-explore-network-directory))
	     (script-call (format "dot %s -T%s -o %s"
			                  (shell-quote-argument gv-file)
			                  file-type
			                  (shell-quote-argument output-file)))
	     (exit-status))
    (message script-call)
    (delete-file output-file)
    (setq exit-status (shell-command script-call))
    (if (eq exit-status 0)
	    (if (file-exists-p output-file)
	        (browse-url-default-browser output-file nil)
	      (user-error "No output file produced"))
      (user-error "GraphViz image generation unsuccessful"))))

(defun matogoro/denote-explore-network-display-gexf (gexf-file)
  "Open the current network GEXF file `gephi'."
  (let* ((gexf-file (expand-file-name (concat denote-explore-network-filename ".gexf") denote-explore-network-directory))
         (script-call (format "gephi %s" (shell-quote-argument gexf-file))))
    (message "Opening %s..." gexf-file)
    (shell-command script-call)
    (message "Opening %s done" gexf-file)))
