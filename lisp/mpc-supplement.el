;;; mpc-supplement.el --- Supplementary customizations for `mpc' -*- no-byte-compile: t; lexical-binding: t; -*-

(defun matogoro/mpc-quick-seek-forward ()
  "Have MPC seek forward +3%."
  (interactive)
  (call-process-shell-command "mpc seek +3% > /dev/null" nil "*Shell Command Output*" t))

(defun matogoro/mpc-quick-seek-backward ()
  "Have MPC seek backward -3%."
  (interactive)
  (call-process-shell-command "mpc seek -3% > /dev/null" nil "*Shell Command Output*" t))

(defun matogoro/mpc-select-toggle-and-move ()
  "Invoke MPC select, then move down one space."
  (interactive)
  (mpc-select-toggle)
  (forward-line 1))

(defun matogoro/mpc-unselect (&optional event)
  "Unselect all selected songs in the current mpc buffer."
  (interactive)
  (cond
   ((get-char-property (point) 'mpc-select)
    (let ((ols nil))
      (dolist (ol mpc-select)
        (if (and (<= (overlay-start ol) (point))
                 (> (overlay-end ol) (point)))
            (delete-overlay ol)
          (push ol ols)))
      (cl-assert (= (1+ (length ols)) (length mpc-select)))
      (setq mpc-select ols)))
   ((mpc-tagbrowser-all-p) nil)
   (t nil))
  (forward-line 1))

(defun matogoro/mpc-unselect-all (&optional event)
  "Unselect all selected songs in the current mpc buffer."
  (interactive)
  (save-excursion
    (goto-char (point-min))
    (while (not (eobp))
      (cond
       ((get-char-property (point) 'mpc-select)
        (let ((ols nil))
          (dolist (ol mpc-select)
            (if (and (<= (overlay-start ol) (point))
                     (> (overlay-end ol) (point)))
                (delete-overlay ol)
              (push ol ols)))
          (cl-assert (= (1+ (length ols)) (length mpc-select)))
          (setq mpc-select ols)))
       ((mpc-tagbrowser-all-p) nil)
       (t nil))
      (forward-line 1))))

(defun matogoro/mpc-add-and-unmark ()
  "Append to playlist, then unmark the song."
  (interactive)
  (mpc-playlist-add)
  (matogoro/mpc-unselect-all))

(defun matogoro/mpc-add-at-point-and-unmark ()
  "Mark, append to playlist, then unmark the song."
  (interactive)
  (mpc-select-toggle)
  (mpc-playlist-add)
  (matogoro/mpc-unselect-all))

(defun matogoro/mpc-move-selected-songs-to-point ()
  "Move selected songs in a playlist to point."
  (interactive)
  (unless mpc-songs-playlist
    (error "The selected songs aren't part of a playlist"))
  (let ((song-poss (mapcar #'cdr (mpc-songs-selection)))
        (dest-pos (get-text-property (point) 'mpc-file-pos)))
    (mpc-cmd-move song-poss dest-pos mpc-songs-playlist)
    (mpc-songs-refresh)
    (matogoro/mpc-unselect-all)
    (message "Moved %d songs" (length song-poss))))
