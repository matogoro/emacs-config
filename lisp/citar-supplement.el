;;; citar-supplement.el --- Supplementary customizations for `citar' -*- no-byte-compile: t; lexical-binding: t; -*-

;;; * Incidator icons

(defvar citar-indicator-files-icon
  (citar-indicator-create
   :symbol (nerd-icons-faicon
            "nf-fa-file_pdf_o"
            :face 'nerd-icons-lred
            :v-adjust -0.0)
   :function #'citar-has-files
   :padding "  " ; need this because the default padding is too low for these icons
   :tag "has:files"))

(defvar citar-indicator-notes-icon
  (citar-indicator-create
   :symbol (nerd-icons-sucicon
            "nf-custom-orgmode"
            :face 'nerd-icons-lgreen
            :v-adjust -0.0)
   :function #'citar-has-notes
   :padding "  "
   :tag "has:notes"))

(defvar citar-indicator-links-icon
  (citar-indicator-create
   :symbol (nerd-icons-codicon
            "nf-cod-globe"
            :face 'nerd-icons-lblue
            :v-adjust -0.0)
   :function #'citar-has-links
   :padding "  "
   :tag "has:links"))

(defvar citar-indicator-cited-icon
  (citar-indicator-create
   :symbol (nerd-icons-mdicon
            "nf-md-file_document_check_outline"
            :face 'nerd-icons-lorange
	        :v-adjust -0.0)
   :function #'citar-is-cited
   :padding "  "
   :tag "is:cited"))

;;; * Jump from PDFs to ZK

(defun matogoro/pdf-to-zk-jump ()
  "When viewing a filed PDF, opens the corresponding ZK note.  If the ZK note does not exist, uses `citar-org-roam' to generate a new ZK note."
  (interactive)
  (if (string= major-mode "pdf-view-mode")
      (if
	      (file-exists-p (concat "~/Documents/Homework/Zettelkasten/Reference/" (file-name-base buffer-file-name)  ".org"))
	      (find-file (concat "~/Documents/Homework/Zettelkasten/Reference/" (file-name-base buffer-file-name)  ".org"))
	    (citar-create-note (file-name-base buffer-file-name)))
    (message "Not in pdf-view-mode.")))
