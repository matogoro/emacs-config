;;; dired-filter.el --- Additional customizations for `dired-filter' -*- no-byte-compile: t; lexical-binding: t; -*-

(setq dired-filter-group-saved-groups
      '(("default"
         ("Archive"
          (extension "zip" "rar" "gz" "bz2" "tar" "xz" "deb" "7z" "apk" "apkg" "xpi" "jar" "war" "ear" "sar"))
         ("Audio"
          (extension "mp3" "flac" "ogg" "oga" "wav" "aac" "m4a" "alac" "wma" "tta" "shn" "sid" "nsf" "mod" "s3m" "vtx" "vgm" "vgz" "psf" "midi" "mpc" "mpp" "mpeg" "gme" "aif" "aiff"))
         ("Document"
          (extension "odt" "odf" "doc" "docx" "rtf" "xml" "csl" "epub" "mobi"))
         ("Data"
          (extension "ods" "xls" "xlsx" "csv" "tsv" "ssv" "accdb" "db" "mdb" "sqlite" "nc"))
         ("Drawing"
          (extension . "odg"))
         ("Encrypted"
          (extension "gpg" "pgp" "asc" "bfe" "enc" "signature" "sig" "p12" "pem"))
         ("Executable"
          (extension "exe" "msi"))
         ("Font"
          (extension "afm" "fon" "fnt" "pfb" "pfm" "ttf" "otf" "woff"))
         ("GIMP"
          (extension . "xcf"))
         ("Image (Raster)"
          (extension "bmp" "jpg" "jpeg" "png" "gif" "apng" "arw" "cr2" "crw" "cur" "dng" "icns" "ico" "jfif" "jp2" "nef" "pbm" "pgm" "ppm" "raf" "tga" "tif" "tiff" "wbmp" "webp" "xbm" "xpm"))
         ("Image (Vector)"
          (extension "svg" "svgz" "eps"))
         ("Log"
          (extension . "log"))
         ("Markdown"
          (extension "etx" "info" "markdown" "md" "mkd" "nfo" "pod" "rst"))
         ("Org"
          (extension . "org"))
         ("Partition"
          (extension "dmg" "iso" "bin" "nrg" "qcow" "toast" "vcd" "vmdk" "bak"))
         ("PDF"
          (extension . "pdf"))
         ("Presentation"
          (extension "odp" "ppt" "pptx"))
         ("Script"
          (extension "R" "ldg" "ledger" "py" "ipynb" "rb" "pl" "t" "msql" "mysql" "pgsql" "sql" "r" "clj" "cljs" "scala" "js"))
         ("Shell"
          (extension "awk" "bash" "bat" "sed" "sh" "zsh" "vim"))
         ("TeX"
          (extension "tex" "bib"))
         ("Text"
          (extension . "txt"))
         ("Version control"
          (extension "git" "gitignore" "gitattributes" "gitmodules"))
         ("Video"
          (extension "f4v" "rmvb" "wvx" "wmx" "wmv" "wm" "asx" "mk3d" "mkv" "fxm" "flv" "axv" "webm" "viv" "yt" "s1q" "smo" "smov" "ssw" "sswf" "s14" "s11" "smpg" "smk" "bk2" "bik" "nim" "pyv" "m4u" "mxu" "fvt" "dvb" "uvvv" "uvv" "uvvs" "uvs" "uvvp" "uvp" "uvvu" "uvu" "uvvm" "uvm" "uvvh" "uvh" "ogv" "m2v" "m1v" "m4v" "mpg4" "mp4" "mjp2" "mj2" "m4s" "3gpp2" "3g2" "3gpp" "3gp" "avi" "mov" "movie" "mpe" "mpeg" "mpegv" "mpg" "mpv" "qt" "vbs"))
         ("Web"
          (extension "css" "less" "sass" "scss" "htm" "html" "jhtm" "mht" "eml" "mustache" "xhtml" "php")))))
