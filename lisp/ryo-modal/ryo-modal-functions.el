;;; ryo-modal-functions.el --- RYO Modal Custom Functions File -*- no-byte-compile: t; lexical-binding: t; -*-

;;; * Definitions and settings

(defvar punct-regex nil "Regex string for listing punctuation marks.")
(setq punct-regex "[\\!\?\"\.'#$%&*+,/:;<=>@^`|~]+")

(defvar word-skip-regex-default nil "Regex string for listing word delimiters.")
(setq word-skip-regex-default "[^]\s\t(){}'\"[★∙∘›»¤∆~⊣¬━∸∷]")

(defvar word-skip-regex-org nil "Regex string for listing word delimiters.")
(setq word-skip-regex-org "[^]\s\t(){}'\"+#*[★∙∘›»¤∆~⊣¬━∸∷|-]")

(defvar clause-regex nil "Regex string for listing sentence clause delimiters.")
(setq clause-regex "\n\\|---\\|[].\"'”’)}»›]  \\|[,;:?!…‽] \\| [([{\"'“‘«‹]")

(setq sentence-end-base "\n\\|[]\"'”’)}»›]  \\|[.?!…‽][]\"'”’)}»›]*")

;;; * Universal argument shortcuts

(defun matogoro/universal-argument-or-more ()
  "Call `universal-argument' or `universal-argument-more',
if there's already a `current-prefix-arg'."
  (interactive)
  (call-interactively
   (if current-prefix-arg
       #'universal-argument-more
     #'universal-argument)))

;;; * Window navigation

(defun matogoro/split-window-right ()
  (interactive)
  (split-window-right)
  (other-window 1))

;;; * In-buffer navigation

(defun matogoro/modified-forward-sentence ()
  (interactive)
  (let (($p (point)))
    (if (and (string= major-mode "org-mode")
	     (string= (org-element-type (org-element-at-point (point))) "table-row"))
	(org-forward-sentence)
      (if (equal (point) (line-end-position))
	  (progn
	    (next-line)
	    (beginning-of-line))
	(forward-sentence)))))

(defun matogoro/beginning-of-visual-line-or-paragraph ()
  (interactive)
  (let (($p (point)))
    (if (or (equal (point) (line-beginning-position))
            (equal last-command this-command))
	(if
	    (progn
	      (backward-char 1)
	      (re-search-backward "\n\n" nil 1 1))
	    (re-search-forward "\n\n")
	  (backward-paragraph))
      (progn
	(when (eq $p (point))
	  (beginning-of-visual-line))))))

(defun matogoro/end-of-visual-line-or-paragraph ()
  (interactive)
  (let (($p (point)))
    (if (or (equal (point) (line-end-position))
            (equal last-command this-command ))
	    (if
	        (progn
	          (forward-char 1)
	          (re-search-forward "\n\n" nil 1 1))
	        (re-search-backward "\n\n")
	      (progn
	        (forward-paragraph)
	        (backward-char 1)))
      (end-of-visual-line))))

(defun matogoro/beginning-of-line-or-paragraph ()
  (interactive)
  (let (($p (point)))
    (if (or (equal (point) (line-beginning-position))
            (equal last-command this-command))
	    (if
	        (progn
	          (backward-char 1)
	          (re-search-backward "\n\n" nil 1 1))
	        (re-search-forward "\n\n")
	      (backward-paragraph))
      (progn
	    (when (eq $p (point))
	      (beginning-of-line))))))

(defun matogoro/end-of-line-or-paragraph ()
  (interactive)
  (let (($p (point)))
    (if (or (equal (point) (line-end-position))
            (equal last-command this-command ))
	    (if
	        (progn
	          (forward-char 1)
	          (re-search-forward "\n\n" nil 1 1))
	        (re-search-backward "\n\n")
	      (progn
	        (forward-paragraph)
	        (backward-char 1)))
      (end-of-line))))

(defun matogoro/backward-word ()
  "Move backward to previous word."
  (interactive)
  (if (string= major-mode "org-mode")
      (re-search-backward word-skip-regex-org nil t)
    (re-search-backward word-skip-regex-default nil t))
  (if (equal (point) (line-end-position))
      (progn
	    (backward-word)
	    (forward-word))
    (progn
      (forward-char)
      (backward-word))))

(defun matogoro/forward-word ()
  "Move forward to next word."
  (interactive)
  (if (string= major-mode "org-mode")
      (re-search-forward word-skip-regex-org nil t)
    (re-search-forward word-skip-regex-default nil t))
  (if (equal (point) (line-beginning-position))
      (progn
	(forward-word)
	(backward-word))
    (progn
      (backward-char)
      (forward-word))))

(defun matogoro/backward-whitespace ()
  "Move backward to previous whitespace."
  (interactive)
  (re-search-backward "[^\s\t]" nil t)
  (if (equal (point) (line-end-position)) nil
    (progn
      (forward-whitespace -1)
      (forward-whitespace 1))))

(defun matogoro/forward-whitespace ()
  "Move forward to next whitespace."
  (interactive)
  (re-search-forward "[^\s\t]" nil t)
  (if (equal (point) (line-beginning-position))
      (progn
	(re-search-forward "[^\s\t]" nil t)
	(backward-char))
    (progn
      (forward-whitespace 1)
      (forward-whitespace -1))))

(defun matogoro/backward-clause (&optional n)
  "Move cursor to the previous clause regex."
  (interactive "p")
  (if (and (string= major-mode "org-mode")
	       (string= (org-element-type (org-element-at-point (point))) "table-row"))
      (progn
        (re-search-backward " | " nil t 1)
        (re-search-backward " | " nil t 1)
        (re-search-forward " | " nil t 1))
    (progn
      (backward-char)
      (re-search-backward clause-regex nil t n)
      (re-search-forward clause-regex nil t 1)
      )))

(defun matogoro/forward-clause (&optional n)
  "Move cursor to the next clause regex."
  (interactive "p")
  (if (and (string= major-mode "org-mode")
	       (string= (org-element-type (org-element-at-point (point))) "table-row"))
      (re-search-forward " | ")
    (progn (forward-char)
	       (re-search-forward clause-regex nil t n)
	       (re-search-backward clause-regex nil t 1))))

(defun matogoro/backward-punct (&optional n)
  "Move cursor to the previous punctuation mark."
  (interactive "p")
  (re-search-backward punct-regex nil t n))

(defun matogoro/forward-punct (&optional n)
  "Move cursor to the next punctuation mark."
  (interactive "p")
  (re-search-forward punct-regex nil t n))

(defun matogoro/end-of-sexp ()
  (interactive)
  (sp-end-of-sexp)
  (forward-char 1))

;;; * Marking and selection

(defun matogoro/pop-local-or-global-mark ()
  "Pop to local mark if it exists, pop or the global mark if it does not."
  (interactive)
  (if (mark t)
      (pop-to-mark-command)
    (pop-global-mark)))

(defun matogoro/xah-select-block ()
  "Select the current/next block plus 1 blank line. If region is active,
extend selection downward by block."
  (interactive)
  (if (region-active-p)
      (re-search-forward "\n[ \t]*\n[ \t]*\n*" nil 1)
    (progn
      (when (= (point) (save-excursion (end-of-line) (point)))
	(backward-char))
      (skip-chars-forward " \n\t")
      (when (re-search-backward "\n[ \t]*\n" nil 1)
        (goto-char (match-end 0)))
      (push-mark (point) t t)
      (re-search-forward "\n[ \t]*\n" nil 1))))

(defun matogoro/select-line-downwards-additive (arg)
  "Select the current line and move the cursor by ARG lines IF
no region is selected.  If a region is already selected when calling this command, only move
the cursor by ARG lines."
  (interactive "p")
  (when (not (use-region-p))
    (forward-line 0)
    (set-mark-command nil))
  (forward-line arg))

(defun matogoro/select-text-in-delimiters ()
  "Select text between the nearest left and right delimiters."
  (interactive)
  (let (start end)
    (skip-chars-backward "^<>([{\"'")
    (setq start (point))
    (skip-chars-forward "^<>)]}\"'")
    (setq end (point))
    (set-mark start)))

;;; * Cut/copy/paste

(defun matogoro/xah-cut-line-or-region ()
  "Cut current line or selection.
   When `universal-argument' is called first, cut whole buffer (respects `narrow-to-region')."
  (interactive)
  (if current-prefix-arg
      (progn
        (kill-new (buffer-string))
        (delete-region (point-min) (point-max)))
    (progn (if (region-active-p)
               (kill-region (region-beginning) (region-end) t)
             (kill-region (line-beginning-position) (line-beginning-position 2))))))

(defun matogoro/xah-copy-line-or-region ()
  "Copy current line or selection.
   When called repeatedly, append copy subsequent lines.
   When `universal-argument' is called first, copy whole buffer (respects `narrow-to-region')."
  (interactive)
  (let ((inhibit-field-text-motion nil))
    (if current-prefix-arg
        (progn
          (copy-region-as-kill (point-min) (point-max)))
      (if (region-active-p)
          (progn
            (copy-region-as-kill (region-beginning) (region-end)))
        (if (eq last-command this-command)
            (if (eobp)
                (progn )
              (progn
                (kill-append "\n" nil)
                (kill-append
                 (buffer-substring-no-properties (line-beginning-position) (line-end-position))
                 nil)
                (progn
                  (end-of-line)
                  (forward-char))))
          (if (eobp)
              (if (eq (char-before) 10 )
                  (progn )
                (progn
                  (copy-region-as-kill (line-beginning-position) (line-end-position))
                  (end-of-line)))
            (progn
              (copy-region-as-kill (line-beginning-position) (line-end-position))
              (end-of-line)
              (forward-char))))))))

(defun matogoro/cut-paragraph ()
  "Cut paragraph at point."
  (interactive)
  (mark-paragraph)
  (kill-region))

(defun matogoro/copy-paragraph ()
  "Copy paragraph at point."
  (interactive)
  (mark-paragraph)
  (kill-ring-save))

(defun matogoro/xah-cut-all-or-region ()
  "Cut the whole buffer content to `kill-ring', or text selection if there is one."
  (interactive)
  (if (use-region-p)
      (progn
        (kill-new (buffer-substring (region-beginning) (region-end)))
        (delete-region (region-beginning) (region-end)))
    (progn
      (kill-new (buffer-string))
      (delete-region (point-min) (point-max)))))

(defun matogoro/xah-copy-all-or-region ()
  "Put the whole buffer content to `kill-ring', or text selection if there is one."
  (interactive)
  (if (use-region-p)
      (progn
        (kill-new (buffer-substring (region-beginning) (region-end)))
        (message "Text selection copied."))
    (progn
      (kill-new (buffer-string))
      (message "Buffer content copied."))))

;;; * Insert/move objects

(defun matogoro/transpose-words-backward ()
  "Wrapper function for transposing words backward."
  (interactive)
  (transpose-words -1))

(defun matogoro/insert-blank-line-above ()
  "Insert an empty line above the current line."
  (interactive)
  (save-excursion
    (end-of-line 0)
    (open-line 1)))

(defun matogoro/insert-blank-line-below ()
  "Insert an empty line below the current line."
  (interactive)
  (save-excursion
    (end-of-line)
    (open-line 1)))

(defun matogoro/xah-move-block-up ()
  "Swap the current text block with the previous.
After this command is called, press <up> or <down> to move. Any other key to exit.
Version 2022-03-04"
  (interactive)
  (let ((xp0 (point))
        xc1 ; current block begin
        xc2 ; current Block End
        xp1 ; prev Block Begin
        xp2 ; prev Block end
        )
    (if (re-search-forward "\n[ \t]*\n+" nil "move")
        (setq xc2 (match-beginning 0))
      (setq xc2 (point)))
    (goto-char xp0)
    (if (re-search-backward "\n[ \t]*\n+" nil "move")
        (progn
          (skip-chars-backward "\n \t")
          (setq xp2 (point))
          (skip-chars-forward "\n \t")
          (setq xc1 (point)))
      (error "No previous block."))
    (goto-char xp2)
    (if (re-search-backward "\n[ \t]*\n+" nil "move")
        (progn
          (setq xp1 (match-end 0)))
      (setq xp1 (point)))
    (transpose-regions xp1 xp2 xc1 xc2)
    (goto-char xp1)
    (set-transient-map
     (let ((xkmap (make-sparse-keymap)))
       (define-key xkmap (kbd "q") #'matogoro/xah-move-block-up)
       (define-key xkmap (kbd "w") #'matogoro/xah-move-block-down)
       xkmap))))

(defun matogoro/xah-move-block-down ()
  "Swap the current text block with the next.
After this command is called, press <up> or <down> to move. Any other key to exit.
Version 2022-03-04"
  (interactive)
  (let ((xp0 (point))
        xc1 ; current block begin
        xc2 ; current Block End
        xn1 ; next Block Begin
        xn2 ; next Block end
        )
    (if (eq (point-min) (point))
        (setq xc1 (point))
      (if (re-search-backward "\n\n+" nil "move")
          (progn
            (setq xc1 (match-end 0)))
        (setq xc1 (point))))
    (goto-char xp0)
    (if (re-search-forward "\n[ \t]*\n+" nil "move")
        (progn
          (setq xc2 (match-beginning 0))
          (setq xn1 (match-end 0)))
      (error "No next block."))
    (if (re-search-forward "\n[ \t]*\n+" nil "move")
        (progn
          (setq xn2 (match-beginning 0)))
      (setq xn2 (point)))
    (transpose-regions xc1 xc2 xn1 xn2)
    (goto-char xn2))
  (set-transient-map
   (let ((xkmap (make-sparse-keymap)))
     (define-key xkmap (kbd "q") #'matogoro/xah-move-block-up)
     (define-key xkmap (kbd "w") #'matogoro/xah-move-block-down)
     xkmap)))

;;; * Killing/changing objects

(defun matogoro/kill-thing-at-point (thing)
  "Kill the `thing-at-point' for the specified kind of THING."
  (let ((bounds (bounds-of-thing-at-point thing)))
    (if bounds
        (kill-region (car bounds) (cdr bounds))
      (error "No %s at point" thing))))

(defun matogoro/kill-word-at-point ()
  "Kill the word at point."
  (interactive)
  (matogoro/kill-thing-at-point 'word))

(defun matogoro/kill-filename-at-point ()
  "Kill the symbol at point."
  (interactive)
  (matogoro/kill-thing-at-point 'filename))

(defun matogoro/kill-url-at-point ()
  "Kill the symbol at point."
  (interactive)
  (matogoro/kill-thing-at-point 'url))

(defun matogoro/kill-retain-line ()
  "Kill the line at point but keep the whitespace."
  (interactive)
  (beginning-of-line)
  (kill-line))

(defun matogoro/xah-delete-current-text-block ()
  "Delete the current text block plus blank lines, or selection, and copy to `kill-ring'."
  (interactive)
  (let ($p1 $p2)
    (if (region-active-p)
        (setq $p1 (region-beginning) $p2 (region-end))
      (progn
        (if (re-search-backward "\n[ \t]*\n+" nil 1)
            (setq $p1 (goto-char (match-end 0)))
          (setq $p1 (point)))
        (re-search-forward "\n[ \t]*\n+" nil 1)
        (setq $p2 (point))))
    (kill-region $p1 $p2)))

;;; * Commenting lines

(defun matogoro/xah-comment-dwim ()
  (interactive)
  (if (region-active-p)
      (comment-dwim nil)
    (let (($lbp (line-beginning-position))
          ($lep (line-end-position)))
      (if (eq $lbp $lep)
          (progn
            (comment-dwim nil))
        (if (eq (point) $lep)
            (progn
              (comment-dwim nil))
          (progn
            (comment-or-uncomment-region $lbp $lep)
            (forward-line )))))))

;;; * Highlighting text

(defun matogoro/highlight-region (@begin @end)
  "Highlight region via overlay.  If no region is selected, highlight the current line."
  (interactive "r")
  (if (region-active-p)
      (progn
	(overlay-put (make-overlay @begin @end) 'face 'highlight)
	(setq mark-active nil))
    (progn
      (overlay-put (make-overlay (line-beginning-position) (line-end-position)) 'face 'highlight)
      (setq mark-active nil)
      (forward-line 1))))

(defun matogoro/remove-overlays-in-region (@begin @end)
  "Remove overlays in region.  If no region is selected, remove overlays on the current line."
  (interactive "r")
  (if (region-active-p)
      (progn
	(remove-overlays @begin @end)
	(setq mark-active nil))
    (progn
      (remove-overlays (line-beginning-position) (line-end-position))
      (setq mark-active nil)
      (forward-line 1))))

;;; * Multiple cursors

(defun matogoro/mc-edit-ends-of-lines ()
  "Modifies mc/edit-ends-of-lines to avoid hanging lines."
  (interactive)
  (if (equal (point) (line-beginning-position))
      (progn
	(backward-char)
	(call-interactively #'mc/edit-ends-of-lines)
	(multiple-cursors-mode)
	)
    (call-interactively #'mc/edit-ends-of-lines)))

;;; * Registers

(defun matogoro/xah-copy-to-register-1 ()
  "Copy current line or selection to register 1."
  (interactive)
  (let (xp1 xp2)
    (if (region-active-p)
        (setq xp1 (region-beginning) xp2 (region-end))
      (setq xp1 (line-beginning-position) xp2 (line-end-position)))
    (copy-to-register ?1 xp1 xp2)
    (message "Copied to register 1: [%s]." (buffer-substring-no-properties xp1 xp2))))

(defun matogoro/xah-append-to-register-1 ()
  "Append current line or selection to register 1."
  (interactive)
  (let (xp1 xp2)
    (if (region-active-p)
        (setq xp1 (region-beginning) xp2 (region-end))
      (setq xp1 (line-beginning-position) xp2 (line-end-position)))
    (append-to-register ?1 xp1 xp2)
    (with-temp-buffer (insert "\n")
                      (append-to-register ?1 (point-min) (point-max)))
    (message "Appended to register 1: [%s]." (buffer-substring-no-properties xp1 xp2))))

(defun matogoro/xah-paste-from-register-1 ()
  "Paste text from register 1."
  (interactive)
  (when (region-active-p)
    (delete-region (region-beginning) (region-end)))
  (insert-register ?1 t))

(defun matogoro/xah-clear-register-1 ()
  "Clear register 1."
  (interactive)
  (progn
    (copy-to-register ?1 (point-min) (point-min))
    (message "Cleared register 1.")))

;;; * Buffer operations

(defun matogoro/copy-buffer-file-name-as-kill (choice)
  "Copyies the buffer {name/mode}, file {name/full path/directory} to the kill-ring."
  (interactive "cCopy: (b) buffer name | (m) buffer major mode | (p) full buffer-file path | (d) buffer-file directory | (f) buffer-file basename")
  (let ((new-kill-string)
        (name (if (eq major-mode 'dired-mode)
                  (dired-get-filename)
                (or (buffer-file-name) ""))))
    (cond ((eq choice ?p)
           (setq new-kill-string name))
          ((eq choice ?d)
           (setq new-kill-string (file-name-directory name)))
          ((eq choice ?f)
           (setq new-kill-string (file-name-nondirectory name)))
          ((eq choice ?b)
           (setq new-kill-string (buffer-name)))
          ((eq choice ?m)
           (setq new-kill-string (format "%s" major-mode)))
          (t (message "Quit")))
    (when new-kill-string
      (message "%s" new-kill-string)
      (kill-new new-kill-string))))

(defun matogoro/save-and-kill-this-buffer ()
  (interactive)
  (save-buffer)
  (kill-this-buffer))

;;; * Miscellaneous editing

(defun matogoro/xah-cycle-hyphen-lowline-space (&optional Begin End)
  "Cycle hyphen/lowline/space chars in selection or inside quote/bracket or line, in that order."
  (interactive)
  ;; this function sets a property 'state. Possible values are 0 to length of xcharArray.
  (let* (xp1
         xp2
         (xcharArray ["-" "_" " "])
         (xn (length xcharArray))
         (xregionWasActive-p (region-active-p))
         (xnowState (if (eq last-command this-command) (get 'matogoro/xah-cycle-hyphen-lowline-space 'state) 0))
         (xchangeTo (elt xcharArray xnowState)))
    (if (and Begin End)
        (setq xp1 Begin xp2 End)
      (if (region-active-p)
          (setq xp1 (region-beginning) xp2 (region-end))
        (let ((xskipChars "^\"<>(){}[]“”‘’‹›«»「」『』【】〖〗《》〈〉〔〕（）"))
          (skip-chars-backward xskipChars (line-beginning-position))
          (setq xp1 (point))
          (skip-chars-forward xskipChars (line-end-position))
          (setq xp2 (point))
          (set-mark xp1))))
    (save-excursion
      (save-restriction
        (narrow-to-region xp1 xp2)
        (goto-char (point-min))
        (while (re-search-forward (elt xcharArray (% (+ xnowState 2) xn)) (point-max) 1)
          (replace-match xchangeTo t t))))
    (when (or (string-equal xchangeTo " ") xregionWasActive-p)
      (goto-char xp2)
      (set-mark xp1)
      (setq deactivate-mark nil))
    (put 'matogoro/xah-cycle-hyphen-lowline-space 'state (% (+ xnowState 1) xn)))
  (set-transient-map (let ((xkmap (make-sparse-keymap))) (define-key xkmap (kbd "p") this-command) xkmap)))

(defun matogoro/xah-toggle-letter-case ()
  "Cycle letter case of words in a text selection."
  (interactive)
  (let (
        (deactivate-mark nil)
        $p1 $p2)
    (if (use-region-p)
        (setq $p1 (region-beginning) $p2 (region-end))
      (save-excursion
        (skip-chars-backward "[:alpha:]")
        (setq $p1 (point))
        (skip-chars-forward "[:alpha:]")
        (setq $p2 (point))))
    (when (not (eq last-command this-command))
      (put this-command 'state 0))
    (cond
     ((equal 0 (get this-command 'state))
      (upcase-initials-region $p1 $p2)
      (put this-command 'state 1))
     ((equal 1 (get this-command 'state))
      (upcase-region $p1 $p2)
      (put this-command 'state 2))
     ((equal 2 (get this-command 'state))
      (downcase-region $p1 $p2)
      (put this-command 'state 0)))))

;;; * Shortcuts

(defun matogoro/jump-to-file-dashboard ()
  "Jump to mini dashboard file."
  (interactive)
  (progn
    (find-file (expand-file-name "~/Documents/Orgzly/DASHBOARD.org"))
    (org-agenda nil "a")
    (previous-window-any-frame)))

(defun matogoro/jump-to-file-caselogs ()
  "Jump to case logs file."
  (interactive)
  (find-file (expand-file-name "~/Documents/Homework/Residency/UCMC/Logs/Cases/caselogs.org")))

(defun matogoro/jump-to-file-ledger-transactions ()
  "Jump to ledger transactions file."
  (interactive)
  (find-file (expand-file-name "~/Documents/Records/Finances/ldgR/data/raw/transactions.ldg")))

(defun matogoro/jump-to-file-cvrules ()
  "Jump to code/verbatim rules file."
  (interactive)
  (find-file (expand-file-name "~/Uniconfig/Settings/Emacs/lisp/org/cvrules.org")))

(defun matogoro/jump-to-file-bookmarks ()
  "Jump to bookmarks file."
  (interactive)
  (find-file (expand-file-name "~/Uniconfig/Settings/Emacs/etc/bookmarks.el")))

(defun matogoro/jump-to-file-projects ()
  "Jump to projects file."
  (interactive)
  (find-file (expand-file-name "~/Uniconfig/Settings/Emacs/etc/projects.el")))

(defun matogoro/reset-to-scratch-buffer ()
  "Reset current frame to a single window showing the scratch buffer."
  (interactive)
  (delete-other-windows)
  (scratch-buffer))

(defun matogoro/dual-pane-dired ()
  "Launch dual-pane `dired' session."
  (interactive)
  (delete-other-windows)
  (dired-jump)
  (matogoro/split-window-right)
  (dired "~/")
  (previous-window-any-frame))

;;; * Word counting

(defun matogoro/simplified-wc-region-or-buffer ()
  "Print number of words and chars in text selection or buffer."
  (interactive)
  (let (p1 p2)
    (if (region-active-p)
        (progn (setq p1 (region-beginning))
               (setq p2 (region-end)))
      (progn (setq p1 (point-min))
             (setq p2 (point-max))))
    (save-excursion
      (let (wCnt charCnt)
        (setq wCnt 0)
        (setq charCnt (- p2 p1))
        (goto-char p1)
        (while (and (< (point) p2) (re-search-forward "\\(\\w\\|\\s_\\)+[^ \t\n]*[ \t\n]*" p2 t))
          (setq wCnt (1+ wCnt)))
        (message "Words: %d | Characters: %d" wCnt charCnt)))))

;;; * Help functions

(defun matogoro/describe-symbol-at-point ()
  (interactive)
  (when-let ((symbol (symbol-at-point)))
    (describe-symbol symbol)))

(defun matogoro/what-cursor-position ()
  (interactive)
  (let ((current-prefix-arg 4))
    (call-interactively 'what-cursor-position)))

;;; * Evaluating elisp

(defun matogoro/eval-region (start end &optional printflag read-function)
  "Modifies `eval-region' to deactivate the mark after the region is evaluated."
  (interactive "r")
  (eval-region start end printflag read-function)
  (deactivate-mark))

(defun matogoro/eval-elisp-block-dwim (start end)
  "Provides a convenient way to quickly evaluate common Emacs lisp code blocks."
  (interactive "r")
  (let ((start (progn (re-search-backward "\n(defun\\|\n(defvar\\|\n(defcustom\\|\n(use-package\\|\n\n(") (forward-char) (point)))
        (end (progn (forward-list) (point))))
    (eval-region start end)
    (message "Elisp block evaluated")))
